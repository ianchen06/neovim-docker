#!/bin/bash
# Command for running neovim

IMAGE="registry.gitlab.com/ianchen06/neovim-docker/master:9889bebbf96bb63f1ebf8d559f2b30f8a313d36a"

if [[ "$1" = /* ]]; then
  file_name="$(basename ${1})"
  dir_name="$(dirname ${1})"
else
  file_name="$1"
  dir_name="$(pwd)"
fi

# Run the docker command
(docker run --name nvim --rm -i -t -P -v "$dir_name":/src ${IMAGE} /bin/sh -c "cd /src;nvim $file_name") 2> /dev/null
