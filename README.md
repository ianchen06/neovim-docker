# neovim

This image contains only the following

1. Python3
1. Neovim 3.1

## Getting Started

1. Put `nvim.sh` to somewhere in your $PATH and make it executable as `nvim`
1. Run the `neovim-docker` container
1. Install your packages (pip3 install -r requirements.txt)
1. Commit the docker image, maybe with the name of the project? (sort of using this like virtualenv, since only the last layer is committed, the neovim layer is reused)
